package com.project.kotlin.service

import com.project.kotlin.dao.MovieDao
import com.project.kotlin.entity.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MovieServiceImpl:MovieService{
    @Autowired
    lateinit var movieDao: MovieDao
    override fun getMovies():List<Movie>{
        return movieDao.getMovies()
    }
}