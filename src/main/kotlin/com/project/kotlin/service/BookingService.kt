package com.project.kotlin.service

import com.project.kotlin.entity.Booking

interface BookingService{
    fun save (booking:Booking):Booking
    fun getHistory(): List<Booking>
}