package com.project.kotlin.service

import com.project.kotlin.dao.UserDataDao
import com.project.kotlin.entity.UserData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserDataServiceImpl:UserDataService{

    @Autowired
    lateinit var userDataDao: UserDataDao
    override fun getAllUser(): List<UserData> {
        return userDataDao.getUser()
    }
    override fun save(userData: UserData): UserData {
        return userDataDao.save(userData)
    }
    override fun saveImage(id: Long, imageUrl: String): UserData {

        val user = userDataDao.findById(id)
        user.image = imageUrl
        return userDataDao.save(user)

    }

}