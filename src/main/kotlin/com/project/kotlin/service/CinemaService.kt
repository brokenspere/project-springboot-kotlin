package com.project.kotlin.service

import com.project.kotlin.entity.Cinema

interface CinemaService{

    fun getCinemas(): List<Cinema>
}