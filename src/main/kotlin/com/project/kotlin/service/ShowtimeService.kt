package com.project.kotlin.service

import com.project.kotlin.entity.Showtime

interface ShowtimeService{
     fun getShowtimes(): List<Showtime>
     fun getShowtimesById(id:Long): Showtime
     fun getMovieShowtimeById(movieId: Long): List<Showtime>

}