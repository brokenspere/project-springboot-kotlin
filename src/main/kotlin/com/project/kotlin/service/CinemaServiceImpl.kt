package com.project.kotlin.service

import com.project.kotlin.dao.CinemaDao
import com.project.kotlin.entity.Cinema
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CinemaServiceImpl:CinemaService{
    @Autowired
    lateinit var cinemaDao: CinemaDao

    override fun getCinemas():List<Cinema>{
        return cinemaDao.getCinema()
    }
}