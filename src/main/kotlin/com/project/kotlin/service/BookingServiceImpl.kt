package com.project.kotlin.service

import com.project.kotlin.dao.BookingDao
import com.project.kotlin.dao.SeatBorDaiDao
import com.project.kotlin.dao.ShowtimeDao
import com.project.kotlin.entity.Booking
import com.project.kotlin.entity.SeatBorDai
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookingServiceImpl:BookingService{


    @Autowired
    lateinit var seatBorDaiDao: SeatBorDaiDao
    @Autowired
    lateinit var showtimeDao: ShowtimeDao
    @Autowired
    lateinit var bookingDao: BookingDao
    override fun save(booking: Booking): Booking {

        var showtimeId = showtimeDao.getShowtimeById(booking.showtimeId!!)
        var seatBorDai = mutableListOf<SeatBorDai>()

        for (item in booking.seats){
            var seat = seatBorDaiDao.findById(item.id)
            seat?.status = true
            seatBorDai.add(seatBorDaiDao.save(seat!!))
        }
        var book = Booking()
        book.showtimeId = showtimeId.id
        book.seats = seatBorDai
        book.reserveDateTime = System.currentTimeMillis()
        bookingDao.save(book)
        return book
    }

    override fun getHistory(): List<Booking> {
        return bookingDao.getHistory()
    }

}