package com.project.kotlin.service

import com.project.kotlin.dao.ShowtimeDao
import com.project.kotlin.entity.Showtime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShowtimeServiceImpl:ShowtimeService{

    @Autowired
    lateinit var showtimeDao: ShowtimeDao
    override fun getShowtimes(): List<Showtime> {
       return showtimeDao.getshowtime()
    }
    override fun getShowtimesById(id: Long): Showtime {
        return showtimeDao.getShowtimeById(id)
    }
    override fun getMovieShowtimeById(movieId: Long): List<Showtime> {
       return showtimeDao.getShowtimeByMovieId(movieId)
    }

}