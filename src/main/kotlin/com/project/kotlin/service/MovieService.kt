package com.project.kotlin.service

import com.project.kotlin.entity.Movie

interface MovieService{

    fun getMovies(): List<Movie>
}