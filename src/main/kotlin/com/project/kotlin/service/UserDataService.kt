package com.project.kotlin.service

import com.project.kotlin.entity.UserData

interface UserDataService{
     fun getAllUser():List<UserData>
     fun save(userData: UserData): UserData
     fun saveImage(id: Long, imageUrl: String): UserData

}