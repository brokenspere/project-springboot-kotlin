package com.project.kotlin.config

import com.project.kotlin.entity.*
import com.project.kotlin.repository.*
import com.project.kotlin.security.entity.Authority
import com.project.kotlin.security.entity.AuthorityName
import com.project.kotlin.security.entity.JwtUser
import com.project.kotlin.security.repository.AuthorityRepository
import com.project.kotlin.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional
import java.sql.Timestamp
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

@Component
class ApplicationLoader:ApplicationRunner{
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var selectedSeatInShowtimeRepository: SelectedSeatInShowtimeRepository
    @Autowired
    lateinit var seatBorDaiRepository: SeatBorDaiRepository
    @Autowired
    lateinit var userDataRepository: UserDataRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name= AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name=AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)

        val encoder = BCryptPasswordEncoder()
        val cust1 = UserData(firstname = "test",email = "a@b.com")
        val custJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = cust1.email,
                enabled = true,
                firstname = cust1.firstname,
                lastname = "test"
        )
        userDataRepository.save(cust1)
        userRepository.save(custJwt)
        cust1.jwtUser = custJwt
        custJwt.user = cust1
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)

    }

    @Transactional
    override fun run (args:ApplicationArguments?) {
        var user1 = userDataRepository.save(UserData("dan@gmail.com","123456","nattapat","tamrakool","imageUrl"))


        var movie1 = movieRepository.save(Movie("อลิตา แบทเทิล แองเจิ้ล", 125, arrayOf("en,th"), "th", "https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024"))
        // movie1.soundtracks= arrayOf("en,th")
        var movie2 = movieRepository.save(Movie("อภินิหารไวกิ้งพิชิตมังกร 3", 105, arrayOf("en,th"), "th", "https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024"))
        // movie2.soundtracks = arrayOf("en,th")
        var movie3 = movieRepository.save(Movie("Captain Marvel", 130, arrayOf("en,th"), "th", "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024"))
        //movie3.soundtracks = arrayOf("en,th")
        var movie4 = movieRepository.save(Movie("เฟรนด์โซน ระวัง..สิ้นสุดทางเพื่อน", 120, arrayOf("th"), "en", "https://lh3.googleusercontent.com/hGX4-13iqHM1Kx_nyj67AtCsFXAjJyS0wxoEcJynOs99BUJXQWLIUQFcsje3jdMW75wtrJU2ktywEF3gxoKe=w1024"))
        //movie4.soundtracks = arrayOf("th")
        var movie5 = movieRepository.save(Movie("สุขสันต์วันตาย 2U", 100, arrayOf("en,th"), "th", "https://lh3.googleusercontent.com/8hKivfDWOC2G8rQolX9850yiPWs5dJzSgyGirJFjU66jeUPV9-5gkm1soLxlHubI5_V_lK1T0vdfinsUlVZtOA=w1024"))
        //moive5.soundtracks = arrayOf("en,th")
        var movie6 = movieRepository.save(Movie("คุซามะ อินฟินิตี้", 80, arrayOf("en"), "th", "https://lh3.googleusercontent.com/Fs3-9D0HjOaP-BWhOhVp-gZAqN3aRyyB5Z7W59f67v_frABpaljFUhFR9Pjs-fTN37jz1mqml9LRUK1tVjjFBg=w1024"))
        //movie6.soundtracks = arrayOf("en")
        var movie7 = movieRepository.save(Movie("คนเหนือมนุษย์", 130, arrayOf("en,th"), "th", "https://lh3.googleusercontent.com/rVCTrkvoC4Iwpv4sPY8gSGYom0rKm28exIaVBD2sGYUip0SMuKqfDxyAVrd_Ps_H39Ss-NGT1nzytQPuBunr=w1024"))
        //movie7.soundtracks = arrayOf("en,th")

        var cinema1 = cinemaRepository.save(Cinema("Cinema 1"))
        var cinema2 = cinemaRepository.save(Cinema("Cinema 2"))
        var cinema3 = cinemaRepository.save(Cinema("Cinema 3"))

        var premiumSeatCinema1 = seatRepository.save(Seat("Premium", SeatType.PREMIUM, 190.00, 3, 22))
        var deluxeSeatCinema1 = seatRepository.save(Seat("Deluxe", SeatType.DELUXE, 150.00, 10, 22))
        var sofaSeatCinema1 = seatRepository.save(Seat("Sofa Sweet (Pair)", SeatType.SOFA, 500.00, 1, 12))

        var premiumSeatCinema2 = seatRepository.save(Seat("Premium", SeatType.PREMIUM, 190.00, 4, 20))
        var deluxeSeatCinema2 = seatRepository.save(Seat("Deluxe", SeatType.DELUXE, 150.00, 10, 20))

        var premiumSeatCinema3 = seatRepository.save(Seat("Premium", SeatType.PREMIUM, 190.00, 1, 18))
        var deluxeSeatCinema3 = seatRepository.save(Seat("Deluxe", SeatType.DELUXE, 150.00, 8, 18))
        var sofaSeatCinema3 = seatRepository.save(Seat("Sofa Sweet (Pair)", SeatType.SOFA, 500.00, 1, 6))

        var showtime1 = showtimeRepository.save(Showtime(Timestamp(1550545200000).time, Timestamp(1550554800000).time, "th", "th"))
        var showtime2 = showtimeRepository.save(Showtime(Timestamp(1550554800000).time,Timestamp(1550561400000).time,"en","th"))
        var showtime3 = showtimeRepository.save(Showtime(Timestamp(1550545200000).time,Timestamp(1550553300000).time,"th","th"))
        var showtime4 = showtimeRepository.save(Showtime(Timestamp(1550553300000).time,Timestamp(1550562900000).time,"en","th"))
        var showtime5 = showtimeRepository.save((Showtime(Timestamp(1550572500000).time,Timestamp(1550581500000).time,"th","en")))
        var showtime6 = showtimeRepository.save(Showtime(Timestamp(1550639100000).time,Timestamp(1550648700000).time,"en","th"))
        var showtime7 = showtimeRepository.save(Showtime(Timestamp(1550655300000).time,Timestamp(1550655300000).time,"th","th"))
        var showtime8 = showtimeRepository.save(Showtime(Timestamp(1550672100000).time,Timestamp(1550681400000).time,"en","th"))

        showtime1.movie = movie7
        showtime1.cinema = cinema2
        showtime2.movie = movie6
        showtime2.cinema = cinema2
        showtime3.movie =  movie2
        showtime3.cinema = cinema3
        showtime4.movie = movie7
        showtime4.cinema = cinema3
        showtime5.movie = movie4
        showtime5.cinema = cinema3
        showtime6.movie = movie3
        showtime6.cinema = cinema3
        showtime7.movie = movie5
        showtime7.cinema = cinema3
        showtime8.movie = movie1
        showtime8.cinema= cinema3

        cinema1.seat = mutableListOf(premiumSeatCinema1, deluxeSeatCinema1, sofaSeatCinema1)
        cinema2.seat = mutableListOf(premiumSeatCinema2, deluxeSeatCinema2)
        cinema3.seat = mutableListOf(premiumSeatCinema3, deluxeSeatCinema3, sofaSeatCinema3)

        loadUsernameAndPassword()

        var row:Char = 'a'
        for ((index,item)in showtime1.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }

        //var row:Char = 'a'
        for ((index,item)in showtime2.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }
        for ((index,item)in showtime3.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }
        for ((index,item)in showtime4.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime4.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }
        for ((index,item)in showtime5.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime5.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }
        for ((index,item)in showtime6.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime6.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }

        for ((index,item)in showtime7.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime7.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }
        for ((index,item)in showtime8.cinema.seat.withIndex()){
            var selectedSeat = selectedSeatInShowtimeRepository.save(SelectedSeatInShowtime(item))
            for(numR in 1..selectedSeat.seatDetail!!.row!!){
                for(numC in 1..selectedSeat.seatDetail!!.column!!){
                    var seat: SeatBorDai
                    if(item.type === SeatType.SOFA){
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    else{
                        seat = seatBorDaiRepository.save(SeatBorDai("R${numR}","C${numC}",false))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime8.seats.add(selectedSeat)
//            if(item.type===SeatType.SOFA){
//                row = 'a'
//            }
        }

    }

}