package com.project.kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProjectBackendApplication

fun main(args: Array<String>) {
    runApplication<ProjectBackendApplication>(*args)
}
