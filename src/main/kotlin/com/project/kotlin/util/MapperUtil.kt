package com.project.kotlin.util

import com.project.kotlin.entity.Cinema
import com.project.kotlin.entity.Movie
import com.project.kotlin.entity.Showtime
import com.project.kotlin.entity.UserData
import com.project.kotlin.entity.dto.*
import com.project.kotlin.security.entity.Authority
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper (componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    fun mapCinemaDto(cinema: Cinema):CinemaDto
    fun mapCinemaDto(cinema:List<Cinema>):List<CinemaDto>

    fun mapShowtimeDto(showtime: Showtime):ShowtimeDto
    fun mapShowtimeDto(showtime: List<Showtime>):List<ShowtimeDto>

    fun mapMovieDto(movie: Movie):MovieDto
    fun mapMoiveDto(movie:List<Movie>):List<MovieDto>

    fun mapUser(user:UserData):UserDataDto
    fun mapUser (user:List<UserData>):List<UserDataDto>

//    fun mapAuthority(authority: Authority): AuthorityDto
//    fun mapAuthority(authority: List<Authority>):List<AuthorityDto>

}