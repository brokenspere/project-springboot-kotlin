package com.project.kotlin.repository

import com.project.kotlin.entity.Cinema
import org.springframework.data.repository.CrudRepository

interface CinemaRepository:CrudRepository<Cinema,Long>{

}