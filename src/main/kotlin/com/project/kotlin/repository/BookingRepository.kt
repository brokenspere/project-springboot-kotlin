package com.project.kotlin.repository

import com.project.kotlin.entity.Booking
import org.springframework.data.repository.CrudRepository

interface BookingRepository:CrudRepository<Booking,Long>{

}