package com.project.kotlin.repository

import com.project.kotlin.entity.Movie
import org.springframework.data.repository.CrudRepository


interface MovieRepository:CrudRepository<Movie,Long>{

}