package com.project.kotlin.repository

import com.project.kotlin.entity.Seat
import org.springframework.data.repository.CrudRepository

interface SeatRepository : CrudRepository<Seat,Long>