package com.project.kotlin.repository

import com.project.kotlin.entity.SeatBorDai
import org.springframework.data.repository.CrudRepository
import java.util.*

interface SeatBorDaiRepository:CrudRepository<SeatBorDai,Long>{
 override   fun findById(id:Long): Optional<SeatBorDai>
}