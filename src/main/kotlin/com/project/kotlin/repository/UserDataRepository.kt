package com.project.kotlin.repository

import com.project.kotlin.entity.UserData
import org.springframework.data.repository.CrudRepository

interface UserDataRepository:CrudRepository<UserData,Long>