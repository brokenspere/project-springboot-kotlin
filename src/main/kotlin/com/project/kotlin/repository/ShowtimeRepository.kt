package com.project.kotlin.repository

import com.project.kotlin.entity.Showtime
import org.springframework.data.repository.CrudRepository

interface ShowtimeRepository:CrudRepository<Showtime,Long>{
     fun findByMovie_Id(movieId: Long): List<Showtime>


}