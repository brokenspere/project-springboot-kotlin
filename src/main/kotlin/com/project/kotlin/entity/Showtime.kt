package com.project.kotlin.entity




import javax.persistence.*

@Entity
data class Showtime(
        var dateStartTime:Long? = null,
        var datteEndTime:Long? = null,
        var soundtrack: String? = null,
        var subtitle: String? = null

)
{
    @Id
    @GeneratedValue
    var id:Long? = null

    @ManyToOne
    var movie:Movie?= null
    @OneToOne
    lateinit var cinema:Cinema
    @OneToMany
    var seats= mutableListOf<SelectedSeatInShowtime>()





}
