package com.project.kotlin.entity

import javax.persistence.*

@Entity
data class Cinema(var name:String?=null){
    @Id
    @GeneratedValue
    var id:Long? = null


    @ManyToMany
    var seat = mutableListOf<Seat>()


}
