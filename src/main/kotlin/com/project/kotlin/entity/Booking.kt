package com.project.kotlin.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToMany

@Entity
data class Booking (
 var reserveDateTime:Long? = null,
 var showtimeId:Long? = null
){
@Id
@GeneratedValue
var id:Long? = null

    @ManyToMany
    var seats = mutableListOf<SeatBorDai>()
}