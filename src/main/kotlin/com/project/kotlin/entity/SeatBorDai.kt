package com.project.kotlin.entity

import javax.persistence.*

@Entity
data class SeatBorDai(
        @Column(name = "SeatBorDai_row")
        var row:String? = null,
        @Column(name = "SeatBorDai_column")
        var column:String? = null,
        var status:Boolean = false
//        @ManyToOne
//        var showtime:Showtime? = null
){
    @Id
    @GeneratedValue
    var id:Long? = null



}