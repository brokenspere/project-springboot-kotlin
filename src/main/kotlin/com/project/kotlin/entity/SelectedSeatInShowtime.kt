package com.project.kotlin.entity

import javax.persistence.*

@Entity
data class SelectedSeatInShowtime(
    @ManyToOne
    var seatDetail:Seat? = null
 ){
    @Id
    @GeneratedValue
    var id:Long? = null



    @OneToMany
    var seats = mutableListOf<SeatBorDai>()
}