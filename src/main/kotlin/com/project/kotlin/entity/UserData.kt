package com.project.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class UserData(
        override var email:String?=null,
        override var password:String?=null,
        var firstname :String? = null,
        var lastname:String? = null,
        var image :String? = null
) : User(email, password){

    @Id
    @GeneratedValue
    override var id:Long? = null

   // @OneToMany
  //var booking:Booking? = null
}