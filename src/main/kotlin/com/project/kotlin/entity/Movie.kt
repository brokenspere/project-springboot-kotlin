package com.project.kotlin.entity

import java.time.Duration
import javax.persistence.*

@Entity
data class Movie(
        var name:String? = null,
        var duration: Int? = null,
        var soundtracks:Array<String>? = null,
        var subtitle:String? = null,
        var image:String? = null
){
    @Id
    @GeneratedValue
    var id:Long?=null


//    @ManyToMany
//    var subtitleMovie = mutableListOf<SubtitleMovie>()
//    @ManyToMany
//    var soundtrackMovie = mutableListOf<SoundtrackMovie>()

}

