package com.project.kotlin.entity

import javax.persistence.*

@Entity
data class Seat(
        var name: String? = null,
        var type :SeatType? = SeatType.DELUXE,
        var price:Double? = null,
        @Column(name = "Seat_row")
        var row:Int? = null,
        @Column(name = "Seat_column")
        var column:Int? = null

){
    @Id
    @GeneratedValue
    var id:Long?=null



}