package com.project.kotlin.entity

enum class SeatType{
    PREMIUM,SOFA,DELUXE
}