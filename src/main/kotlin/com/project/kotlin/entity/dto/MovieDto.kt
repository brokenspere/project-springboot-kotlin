package com.project.kotlin.entity.dto

data class MovieDto(
        var name:String? = null,
        var duration: Int? = null,
        var soundtracks:Array<String>? = null,
        var subtitle:String? = null,
        var image:String? = null,
        var showtime:ShowtimeDto? = null,
        var cinema:CinemaDto? = null

){

}