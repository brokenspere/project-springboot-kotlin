package com.project.kotlin.entity.dto

data class AuthorityDto(var name: String? = null)