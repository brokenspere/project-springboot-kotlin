package com.project.kotlin.entity.dto

data class UserDataDto(
        var email:String?=null,
        var firstname :String? = null,
        var lastname:String? = null,
        var image :String? = null,
        var id:Long? = null
      //  var authorities:List<AuthorityDto> = mutableListOf()
)