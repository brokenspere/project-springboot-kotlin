package com.project.kotlin.entity.dto

data class  ShowtimeDto(
        var dateStartTime:Long? = null,
        var datteEndTime:Long? = null,
        var soundtrack: String? = null,
        var subtitle: String? = null
)