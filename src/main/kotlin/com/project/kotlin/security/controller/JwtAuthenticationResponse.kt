package com.project.kotlin.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)