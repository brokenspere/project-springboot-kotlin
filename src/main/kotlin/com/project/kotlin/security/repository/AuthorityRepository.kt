package com.project.kotlin.security.repository

import com.project.kotlin.security.entity.Authority
import com.project.kotlin.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}