package com.project.kotlin.security.repository

import com.project.kotlin.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}