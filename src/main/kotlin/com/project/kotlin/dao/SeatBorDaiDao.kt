package com.project.kotlin.dao

import com.project.kotlin.entity.SeatBorDai
import org.springframework.stereotype.Repository

interface SeatBorDaiDao{
    fun findById(id:Long?): SeatBorDai?
    fun save(seat: SeatBorDai): SeatBorDai
}