package com.project.kotlin.dao

import com.project.kotlin.entity.UserData

interface UserDataDao{
     fun getUser(): List<UserData>
     fun save(userData: UserData): UserData
     fun findById(id:Long?):UserData
}