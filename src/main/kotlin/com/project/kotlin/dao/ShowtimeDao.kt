package com.project.kotlin.dao

import com.project.kotlin.entity.Showtime

interface ShowtimeDao{
     fun getshowtime(): List<Showtime>
     fun getShowtimeById(id: Long): Showtime
     fun getShowtimeByMovieId(movieId: Long): List<Showtime>

}