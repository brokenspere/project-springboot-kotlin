package com.project.kotlin.dao

import com.project.kotlin.entity.Cinema

interface CinemaDao{

    fun getCinema(): List<Cinema>
}