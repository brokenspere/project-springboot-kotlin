package com.project.kotlin.dao

import com.project.kotlin.entity.Booking
import com.project.kotlin.repository.BookingRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository


@Repository
class BookingDaoImpl:BookingDao{

    @Autowired
    lateinit var bookingRepository: BookingRepository
    override fun getHistory(): List<Booking> {
        return bookingRepository.findAll().filterIsInstance(Booking::class.java)
    }
    override fun save(book: Booking): Booking {
        return bookingRepository.save(book)
    }

}