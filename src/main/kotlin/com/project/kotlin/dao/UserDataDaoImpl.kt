package com.project.kotlin.dao

import com.project.kotlin.entity.UserData
import com.project.kotlin.repository.UserDataRepository
import com.project.kotlin.security.entity.Authority
import com.project.kotlin.security.entity.AuthorityName
import com.project.kotlin.security.entity.JwtUser
import com.project.kotlin.security.repository.AuthorityRepository
import com.project.kotlin.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Repository


@Repository
class UserDataDaoImpl:UserDataDao{

    @Autowired
    lateinit var userDataRepository: UserDataRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var authorityRepository: AuthorityRepository


    override fun findById(id: Long?): UserData {
        return userDataRepository.findById(id!!).orElse(null)
    }

    override fun getUser(): List<UserData> {
        return userDataRepository.findAll().filterIsInstance(UserData::class.java)
    }
    override fun save(userData: UserData): UserData {
        val auth1 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        authorityRepository.save(auth1)
        val encoder = BCryptPasswordEncoder()
        val cust = userData
        val cusJwt = JwtUser(
                username = cust.email,
                password = encoder.encode(cust.password),
                email = cust.email,
                enabled = true,
                firstname = cust.firstname,
                lastname = cust.lastname
        )
        userDataRepository.save(cust)
        userRepository.save(cusJwt)
        cust.jwtUser = cusJwt
        cusJwt.user = cust
        cusJwt.authorities.add(auth1)

        return userDataRepository.save(userData)
    }

}