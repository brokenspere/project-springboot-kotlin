package com.project.kotlin.dao

import com.project.kotlin.entity.Showtime
import com.project.kotlin.repository.ShowtimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class ShowtimeDaoImpl:ShowtimeDao{

    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    override fun getshowtime(): List<Showtime> {
        return showtimeRepository.findAll().filterIsInstance(Showtime::class.java)
    }
    override fun getShowtimeById(id: Long): Showtime {
        return showtimeRepository.findById(id).orElse(null)
    }
    override fun getShowtimeByMovieId(movieId: Long): List<Showtime> {
        return showtimeRepository.findByMovie_Id(movieId)
    }

}