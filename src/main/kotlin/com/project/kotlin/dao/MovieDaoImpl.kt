package com.project.kotlin.dao

import com.project.kotlin.entity.Movie
import com.project.kotlin.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class MovieDaoImpl:MovieDao{
    @Autowired
    lateinit var movieRepository: MovieRepository
    override fun getMovies(): List<Movie> {
        return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }

}