package com.project.kotlin.dao

import com.project.kotlin.entity.Movie

interface MovieDao{
     fun getMovies(): List<Movie>

}