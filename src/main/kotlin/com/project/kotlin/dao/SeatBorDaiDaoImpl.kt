package com.project.kotlin.dao

import com.project.kotlin.entity.SeatBorDai
import com.project.kotlin.repository.SeatBorDaiRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository

class SeatBorDaiDaoImpl:SeatBorDaiDao{
    @Autowired
    lateinit var seatBorDaiRepository: SeatBorDaiRepository
    override fun save(seat: SeatBorDai): SeatBorDai {
        return seatBorDaiRepository.save(seat)
    }

    override fun findById(id: Long?): SeatBorDai? {
        return seatBorDaiRepository.findById(id!!).orElse(null)
    }

}