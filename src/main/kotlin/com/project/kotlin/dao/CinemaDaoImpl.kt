package com.project.kotlin.dao

import com.project.kotlin.entity.Cinema
import com.project.kotlin.repository.CinemaRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class CinemaDaoImpl:CinemaDao{
    @Autowired
    lateinit var cinemaRepository: CinemaRepository

    override fun getCinema():List<Cinema>{
        return cinemaRepository.findAll().filterIsInstance(Cinema::class.java)
    }
}