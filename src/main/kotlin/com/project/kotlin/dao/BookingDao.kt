package com.project.kotlin.dao

import com.project.kotlin.entity.Booking

interface BookingDao{
    fun getHistory(): List<Booking>
    fun save(book: Booking):Booking
}