package com.project.kotlin.controller

import com.project.kotlin.entity.Booking
import com.project.kotlin.service.BookingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController

class BookingController{
    @Autowired
    lateinit var bookingService: BookingService

    @PostMapping("/booking")
    fun Reserve(@RequestBody booking: Booking):ResponseEntity<Any>{
        return ResponseEntity.ok(bookingService.save(booking))
    }

    @GetMapping("/history")
    fun getHistory():ResponseEntity<Any>{
        return ResponseEntity.ok(bookingService.getHistory())
    }
}