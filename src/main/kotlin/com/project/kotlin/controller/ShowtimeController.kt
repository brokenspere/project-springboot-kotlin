package com.project.kotlin.controller

import com.project.kotlin.entity.Movie
import com.project.kotlin.service.ShowtimeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class ShowtimeController{
    @Autowired
    lateinit var showtimeService: ShowtimeService

    @GetMapping("/showtime")
    fun getAllShowtime ():ResponseEntity<Any>{
        return ResponseEntity.ok(showtimeService.getShowtimes())
    }

    @GetMapping("/showtime/{id}")
    fun getOneShowtime(@PathVariable("id")id:Long):ResponseEntity<Any>{
        return ResponseEntity.ok(showtimeService.getShowtimesById(id))
    }

    @GetMapping("/showtime/movie/{movieId}")
    fun getMovieShowtime(@PathVariable movieId:Long):ResponseEntity<Any>{
        return ResponseEntity.ok(showtimeService.getMovieShowtimeById(movieId))
    }
}