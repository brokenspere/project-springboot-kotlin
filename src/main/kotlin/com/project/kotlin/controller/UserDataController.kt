package com.project.kotlin.controller

import com.project.kotlin.entity.UserData
import com.project.kotlin.service.AmazonClient
import com.project.kotlin.service.UserDataService
import com.project.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
class UserDataController{
    @Autowired
    lateinit var userDataService: UserDataService
    @Autowired
    lateinit var amazonClient: AmazonClient

    @GetMapping("/user")
    fun getAllUser():ResponseEntity<Any>{
        val output = userDataService.getAllUser()
        val outputDto = MapperUtil.INSTANCE.mapUser(output)
        return ResponseEntity.ok(outputDto)

       // return ResponseEntity.ok(userDataService.getAllUser())
    }

    @PostMapping("/user")
    @Throws(AuthenticationException::class)
    fun addUser(@RequestBody userData:UserData):ResponseEntity<Any>{
        val output = userDataService.save(userData)
        val outputDto = MapperUtil.INSTANCE.mapUser(output)
        return ResponseEntity.ok(outputDto)

    }

    @PostMapping("/user/image/{userId}")
    fun uploadeImage(@RequestPart(value = "file")file:MultipartFile,
                     @PathVariable("userId")id:Long):ResponseEntity<Any>{
        val imageUrl = this.amazonClient.uploadFile(file)
        val output = userDataService.saveImage(id, imageUrl)
        val ourputDto = MapperUtil.INSTANCE.mapUser(output)
        return ResponseEntity.ok(ourputDto)
      //  return ResponseEntity.ok(userDataService.saveImage(id,imageUrl))
    }

    @PutMapping("/user/{userId}")
    fun updateUser(@PathVariable("userId")id:Long,
                        @RequestBody userData: UserData):ResponseEntity<Any>{
        userData.id = id
        val output = userDataService.save(userData)
        val outputDto = MapperUtil.INSTANCE.mapUser(output)
        return ResponseEntity.ok(outputDto)
    }
}