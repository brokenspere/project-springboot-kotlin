package com.project.kotlin.controller

import com.project.kotlin.service.CinemaService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class CinemaCotroller{
    @Autowired
    lateinit var ciemaService: CinemaService

    @GetMapping("/cinema")
    fun getAllCinema():ResponseEntity<Any>{
        return ResponseEntity.ok(ciemaService.getCinemas())
    }
}