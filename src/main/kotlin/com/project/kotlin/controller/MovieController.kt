package com.project.kotlin.controller

import com.project.kotlin.service.MovieService
import com.project.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MovieController{

    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/movie")
    fun getAllMovies():ResponseEntity<Any>{
        return ResponseEntity.ok(movieService.getMovies())

    }
    @GetMapping("/movie/showtime")
    fun getAllMoviesWithShowtime():ResponseEntity<Any>{
        // return ResponseEntity.ok(movieService.getMovies())
        val movies = movieService.getMovies()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapMoiveDto(movies))
    }

}